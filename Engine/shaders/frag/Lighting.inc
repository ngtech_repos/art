#ifndef LIGHTING_INC
#define LIGHTING_INC 1

/*If not use it - will incorrect background color(green instead black)*/
#define USE_OPTIMIZATION 1

#ifndef BRDF_H
#include "BRDF.h"
#endif /* !BRDF_H */

#ifndef GBUFFER_INC
#include "gbuffer.inc"
#endif // !GBUFFER_INC*/


float GetAngleAttenuation(vec3 _l, vec3 _lightDir, float _angleScale, float _angleOffset)
{
	float cosa = -dot(_l, _lightDir);
	float atten = clamp(cosa * _angleScale + _angleOffset, 0.0, 1.0);

	return atten * atten;
}

/*_gbuffer.Specular - albedo.a */

/* Calculates the color when using a point light. */
vec3 _LightingPoint(in int _index, in vec2 texCoord, in vec3 normal, in GBufferInfo _gbuffer)
{
	vec3 ldir = lights[_index].u_light_pos - _gbuffer.worldPos;
	float dist = length(ldir);
#if USE_OPTIMIZATION
	if (dist > (1 / lights[_index].attIRadius))
		return auto_globalAmbient;
#endif

	vec3 vdir = auto_globalViewPosition - _gbuffer.worldPos;

	vec3 n = normalize(normal);
	vec3 v = normalize(vdir);
	vec3 l = normalize(ldir);
	vec3 h = normalize(v + l);

	float ndotl = clamp(dot(n, l), 0.0, 1.0);
	float ndoth = clamp(dot(n, h), 0.0, 1.0);

	float ndotv = max(dot(n, v), 1e-5);

	float ldoth = clamp(dot(l, h), 0.0, 1.0);
	float vdoth = clamp(dot(v, h), 0.0, 1.0);

	/* Diffuse */
	vec3 Diffuse = Universal_Diffuse(_gbuffer.DiffuseColor, _gbuffer.Roughness, ndotv, ndotl, vdoth, DIFFUSE_ORENNAYAR);

	/* Specular */
	vec3 Specular = BRDF_CookTorrance(_gbuffer.SpecularColor, ldoth, ndoth, ndotv, ndotl, vdoth, _gbuffer.Roughness);

	float dist2 = max(dot(ldir, ldir), 1e-4);
	/* See "Moving Frostbite to PBR" page 30, formula 22 */
	float falloff = (lights[_index].lumIntensity / dist2) * max(0.0, 1.0 - dist * lights[_index].attIRadius);

	float fade = max(0.0, (_gbuffer.Specular - 0.75) * 4.0);
	float shadow = mix(1.0, falloff, fade);

	return (Diffuse * _gbuffer.Specular + Specular) * shadow * ndotl *lights[_index].color;
}

/* TODO: Check NOT READY */
/* Calculates the color when using a directional light. No attenuation for directional lights*/
vec3 _LightingDir(in int _index, in vec2 texCoord, in vec3 normal, in GBufferInfo _gbuffer)
{
	vec3 N = normalize(normal);
	vec3 V = normalize(auto_globalViewPosition - _gbuffer.worldPos);

	vec3 L = normalize(lights[_index].u_light_pos - _gbuffer.worldPos);
	vec3 H = normalize(V + L);

	float NdotH = clamp(dot(N, H), 0.0, 1.0);
	float NdotL = clamp(dot(N, L), 0.0, 1.0);
	float NdotV = clamp(dot(N, V), 0.0, 1.0);
	float HdotV = clamp(dot(H, V), 0.0, 1.0);

	vec3 F0 = vec3(0.04);
	F0 = mix(F0, _gbuffer.DiffuseColor, _gbuffer.Metallic);

	/* reflectance equation */
	vec3 Lo = vec3(0.0);
	{
		/* calculate per-light radiance */
		vec3 L = normalize(lights[_index].u_light_pos - _gbuffer.worldPos);
		vec3 H = normalize(V + L);
		float distance = length(lights[_index].u_light_pos - _gbuffer.worldPos);
		float attenuation = 1.0 / (distance * distance);
		vec3 radiance = lights[_index].color * attenuation;

		// cook-torrance brdf
		float NDF = D_GGX(NdotH, _gbuffer.Roughness);
		float G = G_Smith(NdotL, NdotH, _gbuffer.Roughness);
		vec3 F = F_Schlick(F0, max(HdotV, 0.0));

		vec3 kS = F;
		vec3 kD = vec3(1.0) - kS;
		kD *= 1.0 - _gbuffer.Metallic;

		/* Equal call BRDF_CookTorrance */
		vec3 nominator = NDF * G * F;
		float denominator = 4 * max(dot(N, V), 0.0) * max(NdotL, 0.0) + 0.001;
		vec3 specular = nominator / denominator;

		/* add to outgoing radiance Lo */
		float NdotL = max(NdotL, 0.0);
		Lo += (kD * _gbuffer.DiffuseColor * ONE_OVER_PI + specular) * radiance * NdotL;
	}

	return Lo;
}


/* Calculates the color when using a directional light. No attenuation for directional lights*/
vec3 _LightingSpot(in int _index, in vec2 texCoord, in vec3 normal, in GBufferInfo _gbuffer)
{
	vec3 unormalizedLightVector = lights[_index].u_light_pos - _gbuffer.worldPos;
	float distance = length(unormalizedLightVector);
#if USE_OPTIMIZATION
	if (distance > (1 / lights[_index].attIRadius))
		return auto_globalAmbient;
#endif

	vec3 vdir = auto_globalViewPosition - _gbuffer.worldPos;

	vec3 n = normalize(normal);
	vec3 v = normalize(vdir);
	vec3 l = normalize(unormalizedLightVector);
	vec3 h = normalize(v + l);

	float ndotl = clamp(dot(n, l), 0.0, 1.0);

	float ndoth = clamp(dot(n, h), 0.0, 1.0);

	float ndotv = max(dot(n, v), 1e-5);

	float ldoth = clamp(dot(l, h), 0.0, 1.0);
	float vdoth = clamp(dot(v, h), 0.0, 1.0);

	/* Diffuse */
	vec3 Diffuse = Universal_Diffuse(_gbuffer.DiffuseColor, _gbuffer.Roughness, ndotv, ndotl, vdoth, DIFFUSE_ORENNAYAR);

	/* Specular */
	vec3 Specular = BRDF_CookTorrance(_gbuffer.SpecularColor, ldoth, ndoth, ndotv, ndotl, vdoth, _gbuffer.Roughness);

	float dist2 = max(dot(unormalizedLightVector, unormalizedLightVector), dot(0.1, 0.1));
	float falloff = (lights[_index].lumIntensity / dist2) * max(0.0, 1.0 - distance * lights[_index].attIRadius);

	falloff *= GetAngleAttenuation(l, lights[_index].u_light_dir, lights[_index].angleScale, lights[_index].angleOffset);
#if 0 /* TODO: when SM will be ready */
	float shadow = falloff * VarianceShadow2D(sampler2, ltov, clipPlanes);
#else
	float shadow = falloff * 1.0;
#endif

	float fade = max(0.0, (_gbuffer.Specular - 0.75) * 4.0);

	shadow = mix(1.0, shadow, fade);

	return (Diffuse * _gbuffer.Specular + Specular) * shadow * ndotl * lights[_index].color;
}


vec3 _LightingEnv(in int _index, in vec2 texCoord, in vec3 normal, in GBufferInfo _gbuffer)
{
#define NUM_MIPS	8
	vec3 vdir = auto_globalViewPosition - _gbuffer.worldPos;
	vec3 n = normalize(normal);
	vec3 v = normalize(vdir);

	vec3 r = 2 * dot(v, n) * n - v;

	float ndotv = max(dot(n, v), 1e-5);

	float miplevel = _gbuffer.Roughness * (NUM_MIPS - 1);

	vec3 diffuse_rad = texture(envprobe_diffuse_rad, n).rgb * _gbuffer.DiffuseColor;
	vec3 specular_rad = textureLod(envprobe_specular_rad, r, miplevel).rgb;
	vec2 f0_scale_bias = texture(envprobe_f0_scale_bias, vec2(ndotv, _gbuffer.Roughness)).rg;
	vec3 F = _gbuffer.SpecularColor * f0_scale_bias.x + vec3(f0_scale_bias.y);

	return diffuse_rad * _gbuffer.Specular + specular_rad * F;
}


vec3 ComputeLighting(in int _index, in vec2 _uv, in vec3 _normal, GBufferInfo GBuffer)
{
	vec3 lighting = vec3(0.0);
	switch (lights[_index].type)
	{
	case POINT:
		lighting += _LightingPoint(_index, _uv, _normal, GBuffer);
		break;
	case DIR:
		lighting += _LightingDir(_index, _uv, _normal, GBuffer);
		break;
	case SPOT:
		lighting += _LightingSpot(_index, _uv, _normal, GBuffer);
		break;

	case ENVPROBE:
		lighting += _LightingEnv(_index, _uv, _normal, GBuffer);
		break;
	}
	return lighting;
}

#endif /* LIGHTING_INC */