#include "shared/glslversion.h"
#include "uniforms.inc"


layout(location = 0) out vec3 gRT0;
layout(location = 1) out vec4 gRT1;
layout(location = 2) out vec2 gRT2;
layout(location = 3) out vec4 gRT3;
/* layout(location = 4) out vec4 gRT4;*/

/*
TODO:
1) u_heightmap_texture - Parallax - not implemented
2) Log depth
3)
*/

in VS_OUT
{
	vec3 WorldPos;
	vec3 Tangent;
	vec3 Normal;
	vec3 Bitangent;

	vec2 TexCoords;
	vec2 Depth;
	mat3 TBNmat;
} vs_in;

/**/
uniform sampler2D u_albedo_texture;/*Albedo(Diffuse)*/
uniform sampler2D u_normal_texture;/*Normal*/
uniform sampler2D u_metallic_texture; /*Metallness*/
uniform sampler2D u_roughness_texture; /*Roughness*/
uniform sampler2D u_heightmap_texture; /*HeightMap*/
uniform sampler2D u_aomap_texture; /*AO Map*/
uniform samplerCube u_cubemap_texture; /*CubeMap*/
uniform sampler2D u_basecolor_texture; /*BaseColor Texture*/

/*Parallax height*/
uniform float height_scale;

/*PBR*/
#include "BRDF.h"

/*This params individual for any material*/
uniform vec4 baseColor;
uniform vec4 matParams;
uniform float uIOR;
/*Automatic params - checks what samplers ^ not null*/
uniform int _AlbedoNotNull = 0;
uniform int _NormalNotNull = 0;
uniform int _MetallnessNotNull = 0;
uniform int _RoughnessNotNull = 0;
uniform int _HeightMapNotNull = 0;
uniform int _AONotNull = 0;
uniform int _CubeMapNotNull = 0;
uniform int _BaseColorNotNull = 0;
uniform int _INVERT_Y_UV = 0;

/*Paralax*/
#include "GBuffer.inc"
#include "depth.inc"

uniform vec4 uMaterialAO; /*{ x = uAmbientOcclusionStrength }*/
#if USE_MATERIAL_PARAM
#define uAmbientOcclusionStrength uMaterialAO.x
#else
const float uAmbientOcclusionStrength = 1.0;
#endif


/*Roughness, Metallness must be in [0,1]*/
float remapValueToRangeFromZeroToOne(float _v) {
	return 2.0 * (1.0 / (1.0 - 0.5 + 0.001) - 1.0) * (pow(_v, 2)) + 0.001;
}

void main()
{
	mat3 TBNmat = vs_in.TBNmat;

	/**/
	vec3 TangentViewPos = TBNmat * auto_globalViewPosition;
	vec3 TangentFragPos = TBNmat * vs_in.WorldPos.xyz;
	vec3 eyeV_TBN = normalize(TangentViewPos - TangentFragPos);

	vec3 V = normalize(auto_globalViewPosition - vs_in.WorldPos);
	/**/
	vec2 uv0 = vs_in.TexCoords;

#ifdef INVERT_Y_UV
	uv0.y = 1.0 - uv0.y;
#endif

	vec3 vertexNormal = vs_in.Normal;
	vec3 vertexTangent = vs_in.Tangent;
	vec3 vertexBitangent = vs_in.Bitangent;

	/*Normal mapping*/
	vec3 N = GetNormal(uv0, vertexTangent, vertexBitangent, vertexNormal, u_normal_texture);

	/* And the diffuse per-fragment color */
	/*
	See: https://stackoverflow.com/questions/42238177/check-if-sampler2d-is-empty
	*/
	vec4 _baseColorTmp = vec4(0.0);
	vec4 _albedoTmp = vec4(0.0);
	vec4 _resultColor = vec4(0.0);

	if (_BaseColorNotNull == 1) {
		_baseColorTmp = tosRGB(textureRt(u_basecolor_texture, uv0).xyzw);
	}
	else {
		_baseColorTmp = tosRGB(baseColor);
	}

	if (_AlbedoNotNull == 1) {
		_albedoTmp = tosRGB(textureRt(u_albedo_texture, uv0).xyzw);
	}
	else {
		_albedoTmp = tosRGB(baseColor);
	}


	float diff = 1.0;

#ifdef USE_AO
	vec2 tc = uv0;
	float ao = tosRGBFloat(textureRt(u_aomap_texture, tc).r);
	ao = ao * uAmbientOcclusionStrength + (1.0 - uAmbientOcclusionStrength);
	diff = ao;
#endif

	_albedoTmp *= diff;


	/*matParams = vec4(Roughness, metalness, hastexture, hasnormalmap)*/

	/*Roughness value*/
	float _roughness = 0.0;
	if (_RoughnessNotNull == 1) {
		_roughness = tosRGBFloat(textureRt(u_roughness_texture, uv0).x);
	}
	else {
		_roughness = matParams.x;
	}

	/*Metallness value*/
	float _metallness = 0.0;
	if (_MetallnessNotNull == 1) {
		_metallness = tosRGBFloat(textureRt(u_metallic_texture, uv0).x);
	}
	else {
		_metallness = matParams.y;
	}

	/* Remap values on [0,1]*/
	_roughness = remapValueToRangeFromZeroToOne(_roughness);
	_metallness = remapValueToRangeFromZeroToOne(_metallness);

	/*
	������� �������������� ���� ������ �� ������� �������, �������� �����,
	�������� ������ ���� �� �������� matParams.z = hastexture � _metallness
	*/
	_resultColor = PrepareAlbedoFromAlbedoBaseColorAndParams(_albedoTmp, _baseColorTmp, matParams, _metallness);

	/*Packing GBuffer*/
	GBufferInfo gbuffer;
	{
		/* RT0 */
		{
			gbuffer.BaseColor = _resultColor.xyz;
		}
		/* RT1 */
		{
			gbuffer.Roughness = _roughness;
			gbuffer.Metallic = _metallness;

			/* 
			Fetch material parameters, and conversion to the specular/glossiness model*/
			
			/*glossiness*/
			gbuffer.Specular = _resultColor.w;
			gbuffer.AO = diff;
		}
		
		/* RT2 */
		{
			gbuffer.WorldNormal = pack_normal(EncodeNormal(N));
		}
		
		/* RT3 */
		{
#ifndef _DEVELOPMENT_BUILD
			gbuffer.worldPos = vs_in.WorldPos;
#endif
			gbuffer.LinearDepth = gl_FragCoord.z;
		}
	}


	vec4 empty = vec4(0.0, 0.0, 0.0, 0.0);
	EncodeGBuffer(gbuffer, gRT0, gRT1, gRT2, gRT3, empty/*gRT4*/);
}