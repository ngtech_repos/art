//#if SHADER_ENV
uniform sampler2D u_normal_texture;
uniform samplerCube u_env_map;
//#else
uniform sampler2D u_albedo_texture;
//#endif

//In
in
#include "VertexData.h"

//Out
out vec4 my_FragColor0;


#if GLSL_VERSION > GLSLVERSION_150
layout(early_fragment_tests) in;
#endif

void main(void) {
#if SHADER_ENV
	vec3 normal = 2.0 * texture2D(u_normal_texture, VERTEXDATA.v_tex_coord).xyz - 1.0;
	vec3 env_coord = reflect(VERTEXDATA.v_view_vec, VERTEXDATA.normal);
	my_FragColor0 = texture(u_env_map, env_coord + normal);
#else
	vec4 base = texture(u_albedo_texture, VERTEXDATA.v_tex_coord);
	my_FragColor0 = base*auto_globalambient;
#endif
}