in
#include "VertexData.h"

out vec4 my_FragColor0;

uniform sampler2D u_albedo_texture;/*Diffuse*/
void main()
{
	my_FragColor0 = texture(u_albedo_texture, VERTEXDATA.v_tex_coord);
}