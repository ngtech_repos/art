#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

in
#include "VertexData.h"

#include "LightData.h"

layout(location = 0) out vec4 my_FragColor0;

/*GBuffer*/
layout(binding = 0) uniform sampler2D	gRT0; // A
layout(binding = 1) uniform sampler2D	gRT1; // B
layout(binding = 2) uniform sampler2D	gRT2; // C
layout(binding = 3) uniform sampler2D	gRT3; // D
layout(binding = 4) uniform sampler2D	gRT4; // E
/*Depth Buffer*/
layout(binding = 5) uniform sampler2D	gDepth;

/*ENVPROBE*/
layout(binding = 6) uniform samplerCube	envprobe_diffuse_rad;
layout(binding = 7) uniform samplerCube	envprobe_specular_rad;
layout(binding = 8) uniform sampler2D	envprobe_f0_scale_bias;

#ifndef DNR_LIGHTS
#define DNR_LIGHTS 0
#endif

const int NR_LIGHTS = DNR_LIGHTS + 1;
uniform LightData lights[NR_LIGHTS];

#include "Lighting.inc"

#if GLSL_VERSION > GLSLVERSION_150
layout(early_fragment_tests) in;
#endif

#include "uniforms.inc"
#include "GBuffer.inc"
#include "depth.inc"
#include "tonemapping.inc"

void main()
{
	vec2 uv = VERTEXDATA.v_tex_coord;

	/*GBuffer*/
	GBufferInfo GBuffer;

	DecodeGBufferData(gRT0, gRT1, gRT2, gRT3, gRT4, uv, GBuffer);

	vec3 lighting = vec3(0.0);

	bool bGetNormalizedNormal = true;

	for (int i = 0; i < lights.length(); ++i)
	{
		vec3 normals = unpack_normal(GBuffer.WorldNormal);
		normals = DecodeNormal(normals);

		if (bGetNormalizedNormal) {
			normals = normalize(normals);
		}

		lighting += ComputeLighting
		(i, uv,
			normals,
			GBuffer
		);
	}

	// Ambient Lighting
	lighting += auto_globalAmbient * GBuffer.DiffuseColor * GBuffer.AO;
	// HDR tonemapping
	lighting = lighting / (lighting + vec3(1.0));

	// Defines exposure level for HDR lighting
	GammaCorrection(u_gammaMode, lighting);

	/*DEBUG*/
	int draw_mode = debug_drawmode;

	if (draw_mode == 1)
		my_FragColor0 = vec4(toRGB(GBuffer.DiffuseColor), 1.0);
	else if (draw_mode == 2)
		my_FragColor0 = vec4(unpack_normal(GBuffer.WorldNormal), 1.0);
	else if (draw_mode == 3)
		my_FragColor0 = vec4(GBuffer.worldPos, 1.0);
	else if (draw_mode == 4)
		my_FragColor0 = vec4(vec3(GBuffer.Roughness), 1.0);
	else if (draw_mode == 5)
		my_FragColor0 = vec4(toRGB(GBuffer.SpecularColor), 1.0);
	else if (draw_mode == 6)
		my_FragColor0 = vec4(vec3(GBuffer.Metallic), 1.0);
	else if (draw_mode == 7)
	{
		float _depth = texture(gDepth, uv).x;
		my_FragColor0 = vec4(_depth, _depth, _depth, 1.0);
	}
	else
		my_FragColor0 = vec4(lighting, 1.0);
}