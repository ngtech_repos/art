[GLSL_VERTEX_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

layout(location = 0) in vec3 my_Position;
layout(location = 3) in vec3 my_Normal;
layout(location = 5) in vec2 my_Texcoord0;
layout(location = 6) in vec3 my_Tangent;
layout(location = 7) in vec3 my_Binormal;

out
#include "VertexData.h"

out gl_PerVertex
{
	vec4 gl_Position;
};

out vec3 outUV0;
out vec3 outNormal;
out vec3 outTangent;
out vec3 outPosition;
out vec3 outWorldNormal;

void main()
{
	// Calculate world-space coordinate
	vec3 WorldPos = mat3(auto_globalWorld) * my_Position;

	outNormal = my_Normal;
	outTangent = my_Tangent;
	outWorldNormal = normalize((auto_globalWorld * vec4(my_Normal, 0)).xyz);

	outPosition = (auto_globalMVP * vec4(WorldPos, 1.0)).xyz;
	outUV0 = vec3(my_Texcoord0, -1.0);
	
#if 1
	gl_Position = vec4(outPosition, 1.0);
#else
// on billboard
	gl_Position = vec4(my_Position, 1.0);
#endif
}

[GLSL_FRAGMENT_SHADER]

#version 330 core

layout(location = 0) out vec4 my_FragColor0;

void main(void)
{
	my_FragColor0 = vec4(1.0, 0.0, 0.0, 1.0);
}