#ifndef UNIFORMS_INC
#define UNIFORMS_INC 1

uniform mat4 auto_globalMVPInv;
uniform mat4 auto_globalViewInv;
uniform mat4 auto_globalProjInv;

uniform mat4 auto_globalMVP;
uniform mat4 auto_globalProj;
uniform mat4 auto_globalWorld;
uniform mat4 auto_globalView;

uniform vec3 auto_globalViewPosition;

/*x-NEAR, y-FAR*/
uniform vec2 auto_CameraClipPlanes;

/* Scene Ambient*/
uniform vec3 auto_globalAmbient;

/* Gamma value*/
uniform float u_gammaf;
uniform int u_gammaMode;

/* Screen Size*/
uniform vec2 u_ScreenSize;

/* */
uniform vec4 auto_bakedCameraData;


/*Debug lighting*/
uniform int debug_drawmode;

#endif // UNIFORMS_INC