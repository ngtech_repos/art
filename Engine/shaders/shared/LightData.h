#ifndef LIGHTDATA_H
#define LIGHTDATA_H 1

struct LightData
{
	int type;
	vec3 u_light_pos;
	vec3 u_light_dir;

	vec3 color;

	/*SpotLight*/
	vec2 spotangles;
	float angleScale;
	float angleOffset;

	vec3 lightRight;/*will used in PBR*/
	vec3 lightUp;/*will used in PBR*/

	/*attenation*/
	float attIRadius;

	float lumIntensity;/*will used in PBR*/
	float _luminance;/*PBR, for Area*/
} LIGHTDATA;

#define POINT 1
#define SPOT 2
#define DIR 3
#define ENVPROBE 4
#define AREA 5

#endif //LIGHTDATA_H