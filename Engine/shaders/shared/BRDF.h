#ifndef BRDF_H
#define BRDF_H 1

#include "glslcommon.inc"

/*
baseColor - individual for any material
matParams = vec4(Roughness, metalness, hastexture, hasnormalmap)
*/

/*
Diffuse model
 0: Lambert
 1: Burley
 2: Oren-Nayar
 3: Gotanda*/
#define DIFFUSE_LAMBERT   0
#define DIFFUSE_BURLEY    1
#define DIFFUSE_ORENNAYAR 2
#define DIFFUSE_GOTANDA   3

 /*
 Microfacet distribution function
  0: Blinn
  1: Beckmann
  2: GGX*/
#define PHYSICAL_SPEC_D		2
  /*
  G component
   0: GGX
   1: Walter
   2: Implicit
   3: G_CCT
   4: G_Smith*/
#define PHYSICAL_SPEC_G		0

   /* Forward declarations*/
float D_Blinn(float NoH, float Roughness);
float D_GGX(float NoH, float Roughness);
float D_Beckmann(float NoH, float Roughness);
/* G-Component*/
float G_Component(float NoL, float NoV, float Roughness);

float Vis_Schlick(float NoL, float NoV, float Roughness);
float Vis_Smith(float NoV, float NoL, float Roughness);

float G1_GGX(float NoV, float alphaG);
float Walter(float NoL, float NoV, float Roughness);
float Implicit(float NoL, float NoV, float Roughness);
float G_CCT(float NoH, float VoH, float NoL, float NoV, float Roughness);
float G_Smith(float NoL, float NoV, float Roughness);

vec4 PrepareAlbedoFromAlbedoBaseColorAndParams(in vec4 albedo, in vec4 baseColor, in vec4 matParams);

/*Diffuse*/
vec3 Diffuse_Lambert(vec3 DiffuseColor);
vec3 Diffuse_Burley(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH);
vec3 Diffuse_OrenNayar(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH);
vec3 Diffuse_Gotanda(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH);

/**/
vec3 Diffuse_Lambert(vec3 DiffuseColor)
{
	return DiffuseColor * (1 / PI);
}

/*
[Burley 2012, "Physically-Based Shading at Disney"]
*/
vec3 Diffuse_Burley(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH)
{
	float FD90 = 0.5 + 2 * VoH * VoH * Roughness;
	float FdV = 1 + (FD90 - 1) * Pow5(1 - NoV);
	float FdL = 1 + (FD90 - 1) * Pow5(1 - NoL);
	return DiffuseColor * ((1 / PI) * FdV * FdL);
}

/*
[Gotanda 2012, "Beyond a Simple Physically Based Blinn-Phong Model in Real-Time"]
*/
vec3 Diffuse_OrenNayar(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH)
{
	float a = Roughness * Roughness;
	float s = a;// / ( 1.29 + 0.5 * a );
	float s2 = s * s;
	float VoL = 2 * VoH * VoH - 1;		/* double angle identity*/
	float Cosri = VoL - NoV * NoL;
	float C1 = 1 - 0.5 * s2 / (s2 + 0.33);
	float C2 = 0.45 * s2 / (s2 + 0.09) * Cosri * (Cosri >= 0 ? rcp(max(NoL, NoV)) : 1);
	return DiffuseColor / PI * (C1 + C2) * (1 + Roughness * 0.5);
}

/*
[Gotanda 2014, "Designing Reflectance Models for New Consoles"]
*/
vec3 Diffuse_Gotanda(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH)
{
	float a = Roughness * Roughness;
	float a2 = a * a;
	float F0 = 0.04;
	float VoL = 2 * VoH * VoH - 1;		/* double angle identity*/
	float Cosri = VoL - NoV * NoL;
#if 1
	float a2_13 = a2 + 1.36053;
	float Fr = (1 - (0.542026*a2 + 0.303573*a) / a2_13) * (1 - pow(1 - NoV, 5 - 4 * a2) / a2_13) * ((-0.733996*a2*a + 1.50912*a2 - 1.16402*a) * pow(1 - NoV, 1 + rcp(39 * a2*a2 + 1)) + 1);
	/*float Fr = ( 1 - 0.36 * a ) * ( 1 - pow( 1 - NoV, 5 - 4*a2 ) / a2_13 ) * ( -2.5 * Roughness * ( 1 - NoV ) + 1 );*/
	float Lm = (max(1 - 2 * a, 0) * (1 - Pow5(1 - NoL)) + min(2 * a, 1)) * (1 - 0.5*a * (NoL - 1)) * NoL;
	float Vd = (a2 / ((a2 + 0.09) * (1.31072 + 0.995584 * NoV))) * (1 - pow(1 - NoL, (1 - 0.3726732 * NoV * NoV) / (0.188566 + 0.38841 * NoV)));
	float Bp = Cosri < 0 ? 1.4 * NoV * NoL * Cosri : Cosri;
	float Lr = (21.0 / 20.0) * (1 - F0) * (Fr * Lm + Vd + Bp);
	return DiffuseColor / PI * Lr;
#else
	float a2_13 = a2 + 1.36053;
	float Fr = (1 - (0.542026*a2 + 0.303573*a) / a2_13) * (1 - pow(1 - NoV, 5 - 4 * a2) / a2_13) * ((-0.733996*a2*a + 1.50912*a2 - 1.16402*a) * pow(1 - NoV, 1 + rcp(39 * a2*a2 + 1)) + 1);
	float Lm = (max(1 - 2 * a, 0) * (1 - Pow5(1 - NoL)) + min(2 * a, 1)) * (1 - 0.5*a + 0.5*a * NoL);
	float Vd = (a2 / ((a2 + 0.09) * (1.31072 + 0.995584 * NoV))) * (1 - pow(1 - NoL, (1 - 0.3726732 * NoV * NoV) / (0.188566 + 0.38841 * NoV)));
	float Bp = Cosri < 0 ? 1.4 * NoV * Cosri : Cosri / max(NoL, 1e-8);
	float Lr = (21.0 / 20.0) * (1 - F0) * (Fr * Lm + Vd + Bp);
	return DiffuseColor / PI * Lr;
#endif
}

vec3 Universal_Diffuse(vec3 DiffuseColor, float Roughness, float NoV, float NoL, float VoH, int Model)
{
	if (Model == DIFFUSE_GOTANDA)
		return Diffuse_Gotanda(DiffuseColor, Roughness, NoV, NoL, VoH);
	else if (Model == DIFFUSE_ORENNAYAR)
		return Diffuse_OrenNayar(DiffuseColor, Roughness, NoV, NoL, VoH);
	else if (Model == DIFFUSE_BURLEY)
		return Diffuse_Burley(DiffuseColor, Roughness, NoV, NoL, VoH);
	else
		return Diffuse_Lambert(DiffuseColor);
}

/*
[Blinn 1977, "Models of light reflection for computer synthesized pictures"]
*/
float D_Blinn(float NoH, float Roughness)
{
	float a = Roughness * Roughness;
	float a2 = a * a;
	float n = 2 / a2 - 2;
	return (n + 2) / (2 * PI) * PhongShadingPow(NoH, n);		/* 1 mad, 1 exp, 1 mul, 1 log*/
}

/*
[Beckmann 1963, "The scattering of electromagnetic waves from rough surfaces"]
*/
float D_Beckmann(float NoH, float Roughness)
{
	float a = Roughness * Roughness;
	float a2 = a * a;
	NoH = max(NoH, 0.0);
	float NoH2 = NoH * NoH;
	return exp((NoH2 - 1.0) / (a2 * NoH2)) / (PI * a2 * NoH2 * NoH2);
}

/*
GGX / Trowbridge-Reitz
[Walter et al. 2007, "Microfacet models for refraction through rough surfaces"]
*/
float D_GGX(float ndoth, float Roughness)
{
	float a = max(Roughness * Roughness, 2e-3);
	float a2 = a * a;
	/*microfacet distribution*/
	float d = ndoth * ndoth * (a2 - 1.0) + 1.0; /* 2 mad*/
	return a2 / (PI * d * d);					/* 4 mul, 1 rcp*/
}

/*
Anisotropic GGX
[Burley 2012, "Physically-Based Shading at Disney"]
*/
float D_GGXaniso(float RoughnessX, float RoughnessY, float NoH, vec3 H, vec3 X, vec3 Y)
{
	float ax = RoughnessX * RoughnessX;
	float ay = RoughnessY * RoughnessY;
	float XoH = dot(X, H);
	float YoH = dot(Y, H);
	float d = XoH * XoH / (ax*ax) + YoH * YoH / (ay*ay) + NoH * NoH;
	return 1 / (PI * ax*ay * d*d);
}

/* TODO: DEPRECATED*/
float Distribution(float NoH, float Roughness)
{
#if   PHYSICAL_SPEC_D == 0
	return D_Blinn(NoH, Roughness);
#elif PHYSICAL_SPEC_D == 1
	return D_Beckmann(NoH, Roughness);
#elif PHYSICAL_SPEC_D == 2
	return D_GGX(NoH, Roughness);
#endif
}

float Vis_Implicit()
{
	return 0.25;
}

/* [Neumann et al. 1999, "Compact metallic reflectance models"] */
float Vis_Neumann(float NoV, float NoL)
{
	return 1 / (4 * max(NoL, NoV));
}

/*
[Kelemen 2001, "A microfacet based coupled specular-matte brdf model with importance sampling"]
*/
float Vis_Kelemen(float VoH)
{
	/* constant to prevent NaN */
	return rcp(4 * VoH * VoH + 1e-5);
}

vec3 F_None(vec3 SpecularColor)
{
	return SpecularColor;
}

/*
[Schlick 1994, "An Inexpensive BRDF Model for Physically-Based Rendering"]
*/
vec3 F_Schlick(vec3 SpecularColor, float VoH)
{
	float Fc = pow(1.0 - VoH, 5.0);				/* 1 sub, 3 mul*/

	/* Anything less than 2% is physically impossible and is instead considered to be shadowing*/
	return saturate(50.0 * SpecularColor.g) * Fc + (1 - Fc) * SpecularColor;
}

vec3 F_Fresnel(vec3 SpecularColor, float VoH)
{
	vec3 SpecularColorSqrt = sqrt(clamp(vec3(0, 0, 0), vec3(0.99, 0.99, 0.99), SpecularColor));
	vec3 n = (1 + SpecularColorSqrt) / (1 - SpecularColorSqrt);
	vec3 g = sqrt(n*n + VoH * VoH - 1);
	return 0.5 * Square((g - VoH) / (g + VoH)) * (1 + Square(((g + VoH)*VoH - 1) / ((g - VoH)*VoH + 1)));
}

/*
===========
geometry
===========
*/
float G_Component(float ndotl, float ndotv, float ndoth, float vdoth, float Roughness)
{
#if   PHYSICAL_SPEC_G == 0
	return Vis_Schlick(ndotl, ndotv, Roughness);
#elif PHYSICAL_SPEC_G == 1
	return Walter(ndotl, ndotv, Roughness);
#elif PHYSICAL_SPEC_G == 2
	return Implicit(ndotl, ndotv, Roughness);
#elif PHYSICAL_SPEC_G == 3
	return G_CCT(ndoth, vdoth, ndotl, ndotv, Roughness);
#elif PHYSICAL_SPEC_G == 4
	return G_Smith(ndotl, ndotv, Roughness);
#endif
}
float G_CCT(float NdotH, float VdotH, float ndotl, float ndotv, float Roughness) {
	float NdotL_clamped = max(ndotl, 0.0);

	float NdotV_clamped = max(ndotl, 0.0);

	return min(min(2.0 * NdotH * NdotV_clamped / VdotH, 2.0 * NdotH * NdotL_clamped / VdotH), 1.0);
}

/*
Tuned to match behavior of Vis_Smith
[Schlick 1994, "An Inexpensive BRDF Model for Physically-Based Rendering"] */
float Vis_Schlick(float ndotl, float ndotv, float Roughness)
{
#if 1 // UE4
	float k = Square(Roughness) * 0.5;
	float Vis_SchlickV = ndotv * (1 - k) + k;
	float Vis_SchlickL = ndotl * (1 - k) + k;
	return 0.25 / (Vis_SchlickV * Vis_SchlickL);
#else
	/*Nick: is better image, but is equal and math accuracity(see form)*/
	float NdotL_clamped = max(ndotl, 0.0);
	float NdotV_clamped = max(ndotv, 0.0);
	float k = Roughness * sqrt(2.0 / 3.14159265);
	float one_minus_k = 1.0 - k;
	return (NdotL_clamped / (NdotL_clamped * one_minus_k + k)) * (NdotV_clamped / (NdotV_clamped * one_minus_k + k));
#endif
}

/*
Smith term for GGX
[Smith 1967, "Geometrical shadowing of a random rough surface"]*/
float Vis_Smith(float NoV, float NoL, float Roughness)
{
	float a = Square(Roughness);
	float a2 = a * a;

	float Vis_SmithV = NoV + sqrt(NoV * (NoV - NoV * a2) + a2);
	float Vis_SmithL = NoL + sqrt(NoL * (NoL - NoL * a2) + a2);
	return rcp(Vis_SmithV * Vis_SmithL);
}

/*
Appoximation of joint Smith term for GGX
[Heitz 2014, "Understanding the Masking-Shadowing Function in Microfacet-Based BRDFs"]
*/
float Vis_SmithJointApprox(float NoV, float NoL, float Roughness)
{
	float a = Square(Roughness);
	float Vis_SmithV = NoL * (NoV * (1 - a) + a);
	float Vis_SmithL = NoV * (NoL * (1 - a) + a);
	return 0.5 * rcp(Vis_SmithV + Vis_SmithL);
}

/* used in Walter */
float G1_GGX(float Ndotv, float alphaG)
{
	return 2 / (1 + sqrt(1 + alphaG * alphaG * (1 - Ndotv * Ndotv) / (Ndotv*Ndotv)));
}

float Walter(float ndotl, float ndotv, float Roughness)
{
	float a = 1.0 / (Roughness * tan(acos(ndotv)));
	float a_Sq = a * a;
	float a_term;
	if (a < 1.6)
		a_term = (3.535 * a + 2.181 * a_Sq) / (1.0 + 2.276 * a + 2.577 * a_Sq);
	else
		a_term = 1.0;

	return G1_GGX(ndotl, a_term)*G1_GGX(ndotv, a_term);
}

float Implicit(float ndotl, float ndotv, float Roughness) {
	return max(ndotl, 0.0) * max(ndotv, 0.0);
}

/*
 Smith term for GGX modified by Disney to be less "hot" for small Roughness values
 [Smith 1967, "Geometrical shadowing of a random rough surface"]
 [Burley 2012, "Physically-Based Shading at Disney"]
*/
float G_Smith(float ndotl, float ndotv, float Roughness)
{
	float a = Roughness * Roughness;
	float a2 = a * a;

	float G_SmithV = ndotv + sqrt(ndotv * (ndotv - ndotv * a2) + a2);
	float G_SmithL = ndotl + sqrt(ndotl * (ndotl - ndotl * a2) + a2);
	return rcp(G_SmithV * G_SmithL);
}

//=====================================
/*
matParams = vec4(Roughness, metalness, hastexture, hasnormalmap)
*/
vec4 PrepareAlbedoFromAlbedoBaseColorAndParams(in vec4 albedo, in vec4 baseColor, in vec4 matParams, in float _metalness)
{
	vec4 result = mix(baseColor, albedo, matParams.z);
	result.rgb = mix(result.rgb, vec3(0.0), _metalness);

	return result;
}

vec3 BRDF_CookTorrance(in vec3 F0, float ldoth, float ndoth, float ndotv, float ndotl, float vdoth, float Roughness)
{
	vec3 F = F_Schlick(F0, ldoth);
	float Vis = G_Component(ndotl, ndotv, ndoth, vdoth, Roughness);
	float D = Distribution(ndoth, Roughness);

	return F * Vis * D;
}

/*
---------------
 EnvBRDF
---------------
*/
float EnvBRDFApproxNonmetal(float Roughness, float NoV)
{
	/* Same as EnvBRDFApprox( 0.04, Roughness, NoV )*/
	const vec2 c0 = { -1, -0.0275 };
	const vec2 c1 = { 1, 0.0425 };
	vec2 r = Roughness * c0 + c1;
	return min(r.x * r.x, exp2(-9.28 * NoV)) * r.x + r.y;
}

float D_InvBlinn(float Roughness, float NoH)
{
	float m = Roughness * Roughness;
	float m2 = m * m;
	float A = 4;
	float Cos2h = NoH * NoH;
	float Sin2h = 1 - Cos2h;
	/*return rcp( PI * (1 + A*m2) ) * ( 1 + A * ClampedPow( Sin2h, 1 / m2 - 1 ) ); */
	return rcp(PI * (1 + A * m2)) * (1 + A * exp(-Cos2h / m2));
}

float D_InvBeckmann(float Roughness, float NoH)
{
	float m = Roughness * Roughness;
	float m2 = m * m;
	float A = 4;
	float Cos2h = NoH * NoH;
	float Sin2h = 1 - Cos2h;
	float Sin4h = Sin2h * Sin2h;
	return rcp(PI * (1 + A * m2) * Sin4h) * (Sin4h + A * exp(-Cos2h / (m2 * Sin2h)));
}

float D_InvGGX(float Roughness, float NoH)
{
	float a = Roughness * Roughness;
	float a2 = a * a;
	float A = 4;
	float d = (NoH - a2 * NoH) * NoH + a2;
	return rcp(PI * (1 + A * a2)) * (1 + 4 * a2*a2 / (d*d));
}

float Vis_Cloth(float NoV, float NoL)
{
	return rcp(4 * (NoL + NoV - NoL * NoV));
}

#endif