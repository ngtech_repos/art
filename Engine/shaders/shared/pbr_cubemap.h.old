#ifndef  USE_CUBEMAP_BLUR
#define  USE_CUBEMAP_BLUR 1

//Shamelessly taken from TDR : https://www.shadertoy.com/view/XsfXWX
float somestep(float t) {
	return pow(t, 4.0);
}

vec3 textureAVG(samplerCube tex, vec3 tc) {
	const float diff0 = 0.35;
	const float diff1 = 0.12;
	vec3 s0 = texture(tex, tc).xyz;
	vec3 s1 = texture(tex, tc + vec3(diff0)).xyz;
	vec3 s2 = texture(tex, tc + vec3(-diff0)).xyz;
	vec3 s3 = texture(tex, tc + vec3(-diff0, diff0, -diff0)).xyz;
	vec3 s4 = texture(tex, tc + vec3(diff0, -diff0, diff0)).xyz;

	vec3 s5 = texture(tex, tc + vec3(diff1)).xyz;
	vec3 s6 = texture(tex, tc + vec3(-diff1)).xyz;
	vec3 s7 = texture(tex, tc + vec3(-diff1, diff1, -diff1)).xyz;
	vec3 s8 = texture(tex, tc + vec3(diff1, -diff1, diff1)).xyz;

	return (s0 + s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8) * 0.111111111;
}

vec3 textureBlured(samplerCube tex, vec3 tc) {
	vec3 r = textureAVG(tex, vec3(1.0, 0.0, 0.0));
	vec3 t = textureAVG(tex, vec3(0.0, 1.0, 0.0));
	vec3 f = textureAVG(tex, vec3(0.0, 0.0, 1.0));
	vec3 l = textureAVG(tex, vec3(-1.0, 0.0, 0.0));
	vec3 b = textureAVG(tex, vec3(0.0, -1.0, 0.0));
	vec3 a = textureAVG(tex, vec3(0.0, 0.0, -1.0));

	float kr = dot(tc, vec3(1.0, 0.0, 0.0)) * 0.5 + 0.5;
	float kt = dot(tc, vec3(0.0, 1.0, 0.0)) * 0.5 + 0.5;
	float kf = dot(tc, vec3(0.0, 0.0, 1.0)) * 0.5 + 0.5;
	float kl = 1.0 - kr;
	float kb = 1.0 - kt;
	float ka = 1.0 - kf;

	kr = somestep(kr);
	kt = somestep(kt);
	kf = somestep(kf);
	kl = somestep(kl);
	kb = somestep(kb);
	ka = somestep(ka);

	float d;
	vec3 ret;
	ret = f * kf; d = kf;
	ret += a * ka; d += ka;
	ret += l * kl; d += kl;
	ret += r * kr; d += kr;
	ret += t * kt; d += kt;
	ret += b * kb; d += kb;

	return ret / d;
}

#endif