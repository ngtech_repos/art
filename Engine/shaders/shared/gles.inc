#ifdef GL_ES
precision highp float;
precision highp int;

precision highp vec2;
precision highp vec3;
precision highp vec4;

precision highp uvec2;
precision highp uvec3;
precision highp uvec4;

precision lowp sampler2D;
precision lowp usampler2D;
#endif