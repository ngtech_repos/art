#ifndef DEBUG_COLORS_INC
#define DEBUG_COLORS_INC 1

#define RED_COLOR vec4(1.0,0.0,0.0,1.0)
#define GREEN_COLOR vec4(0.0,1.0,0.0,1.0)
#define BLUE_COLOR vec4(0.0,0.0,1.0,1.0)
#define YELLOW_COLOR vec4(1.0,1.0,0.0,1.0)
#define BLACK_COLOR vec4(0.0,0.0,0.0,1.0)
#define WHITE_COLOR vec4(1.0,1.0,1.0,1.0)

#endif //DEBUG_COLORS_INC