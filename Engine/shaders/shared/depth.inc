#ifndef DEPTH_INC
#define DEPTH_INC 1

float LinearizeDepth(float depth, float _near, float _far )
{
	/*Default values _near = 0.1, _far = 100.0*/
	float z = depth * 2.0 - 1.0; // Back to NDC 
	return (2.0 * _near * _far) / (_far + _near - z * (_far - _near));
}

vec4 ReconstructWorldPosFromDepth(in float _depth, in vec2 _uv, in mat4 _inverseMVP)
{
	_depth = _depth * 2.0 - 1.0;

	vec4 viewSpacePos = vec4(_uv * 2.0 - vec2(1.0), _depth, 1.0);
	vec4 worldPos = _inverseMVP * viewSpacePos; 

	worldPos /= worldPos.w;

	return worldPos;
}

#endif //DEPTH_INC