#ifndef NORMALS_INC
#define NORMALS_INC 1

/*
Compute normal.z
found in http://www.slideshare.net/franknine/deferred-shading-10264624 25 slide
and http://gamedevs.org/uploads/stalker-deferred-rendering-direct3d10-showcase.pdf
*/

vec3 unpack_normal(in vec2 norm)
{
	vec3 res;
	res.xy = (2.0*abs(norm)) - vec2(1, 1);
	res.z = (norm.x < 0 ? -1.0 : 1.0)*sqrt(abs(1 - res.x*res.x - res.y*res.y));
	return res;
}

vec2 pack_normal(in vec3 norm)
{
	vec2 res;
	res = 0.5 *(norm.xy +
		vec2(1, 1));
	res.x *= (norm.z < 0 ? -1.0 : 1.0);
	return res;
}

vec3 BumpNormal(vec3 _normal)
{
	return (_normal * 2.0 - 1.0);
}

vec3 GetNormal(vec2 uv, inout vec3 vertexTangent, inout vec3 vertexBitangent, inout vec3 vertexNormal, in sampler2D _sampler)
{
	/*sample and scale/bias the normal map*/
	vec3 n = BumpNormal(texture(_sampler, uv).xyz);

	/* TODO: TANGENT NOT IMPLEMENTED YET*/

	vec3 T = vertexTangent;
	vec3 N = vertexNormal;
	vec3 B = vertexBitangent;

	/* ortho-normalization */
	float renormalize = 1.0;
	float orthogonalize = 1.0;
	float regenBitangent = 1.0;

	N = mix(N, normalize(N), renormalize);
	T -= (orthogonalize * dot(T, N)) * N;
	T = mix(T, normalize(T), renormalize);
	B -= orthogonalize * (dot(B, N)*N + dot(B, T)*T);
	B = mix(B, normalize(B), renormalize);

	/* regenerate bitangent */
	vec3 B2 = cross(N, T);
	B2 = dot(B2, B) < 0.0 ? -B2 : B2;
	B = mix(B, B2, regenBitangent);

	vertexTangent = T;
	vertexBitangent = B;
	vertexNormal = N;

	/* store our results */
	return normalize(n.x*T + n.y*B + n.z*N);
}

vec3 EncodeNormal(vec3 N)
{
	return N * 0.5 + 0.5;
}

vec3 DecodeNormal(vec3 N)
{
	return N * 2 - 1;
}

#endif //NORMALS_INC