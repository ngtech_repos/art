#ifndef GLSLCOMMON_INC
#define GLSLCOMMON_INC 1

#include "gles.inc"

const float EPSILON = 0.000001;
const float ONE_OVER_PI = 0.318309;
const float PI = 3.1415926535897932;
const float TWOPI = 6.2831853071795864;
const float QUAD_PI = 12.566370614359172;
const int	NUM_MIPS = 8;

/* Read from a render target texture*/
/*#define textureRt(tex_, texc_) texture(tex_, texc_) */
#define textureRt(tex_, texc_) textureLod(tex_, texc_, 0.0)

float rcp(float x)
{
	return 1.0 / x;
}

/* Used only for GLSL
round->floor
lerp->mix
saturate(x)-> clamp(x,0.0,1.0)
Square(x)-> x*x
*/
#define lerp(x,y,z) mix(x,y,z)
#define saturate(x) clamp(x,0.0,1.0)
#define Square(x) x*x


/***/
vec3 rsqrt(vec3 a)
{
	return pow(a, vec3(-0.5));
}

vec3 toRGBM(vec3 srgb)
{
	return pow(srgb, vec3(/*1.0 / 1.2*/0.83));
}

vec3 toRGB(vec3 srgb)
{
	return pow(srgb, vec3(/*1.0 / 2.2*/0.45));
}

float tosRGBFloat(float rgba)
{
	float srgb = (rgba*rgba)*(rgba*0.2848 + 0.7152);
	return srgb;
}

vec4 tosRGB(vec4 rgba)
{
	vec3 srgb = (rgba.xyz*rgba.xyz)*(rgba.xyz*vec3(0.2848, 0.2848, 0.2848) + vec3(0.7152, 0.7152, 0.7152));
	return vec4(srgb, rgba.a);
}

vec3 tosRGB(vec3 rgb)
{
	vec3 srgb = (rgb.xyz*rgb.xyz)*(rgb.xyz*vec3(0.2848, 0.2848, 0.2848) + vec3(0.7152, 0.7152, 0.7152));
	return srgb;
}

/*
This function for construction TBN matrix from normal and tangent vectors
*/
mat3 ConstructTBN(vec3 _normal, vec3 _tangent)
{
	vec3 T = normalize(_tangent);
	vec3 N = normalize(_normal);
	vec3 B = cross(N, T);
	mat3 TBNmat = transpose(mat3(T, B, N));
	return TBNmat;
}


float ClampedPow(float X, float Y)
{
	return pow(max(abs(X), 0.000001), Y);
}

/**
* Use this function to compute the pow() in the specular computation.
* This allows to change the implementation depending on platform or it easily can be replaced by some approxmation.
*/
float PhongShadingPow(float X, float Y)
{
	/* 
	The following clamping is done to prevent NaN being the result of the specular power computation.
	Clamping has a minor performance cost.

	In HLSL pow(a, b) is implemented as exp2(log2(a) * b).

	For a=0 this becomes exp2(-inf * 0) = exp2(NaN) = NaN.

	As seen in #TTP 160394 "QA Regression: PS3: Some maps have black pixelated artifacting."
	this can cause severe image artifacts (problem was caused by specular power of 0, lightshafts propagated this to other pixels).
	The problem appeared on PlayStation 3 but can also happen on similar PC NVidia hardware.

	In order to avoid platform differences and rarely occuring image atrifacts we clamp the base.

	Note: Clamping the exponent seemed to fix the issue mentioned TTP but we decided to fix the root and accept the
	minor performance cost.
	*/

	return ClampedPow(X, Y);
}

float Pow5(float x)
{
	float x2 = x*x;
	return x2 * x2 * x;
}

/**/
// TODO:SHADERS:Nick: not implemented UBO View
//float ConvertFromDeviceZ(float DeviceZ)
//{
//	return 1.f / (DeviceZ * View.InvDeviceZToWorldZTransform[2] - View.InvDeviceZToWorldZTransform[3]);
//}



#endif