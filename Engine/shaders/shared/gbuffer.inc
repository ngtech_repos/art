#ifndef GBUFFER_INC
#define GBUFFER_INC 1

/*Normals packing/unpacking*/
#include "Normals.inc"
#include "Pack.inc"

#if USE_FRAMEBUFFER_SRGB

vec3 EncodeBaseColor(vec3 BaseColor)
{
	/* we use sRGB on the render target to give more precision to the darks*/
	return BaseColor;
}

vec3 DecodeBaseColor(vec3 BaseColor)
{
	/* we use sRGB on the render target to give more precision to the darks*/
	return BaseColor;
}

#else /* @todo: remove once Apple fixes radr://16754329 AMD Cards don't always perform FRAMEBUFFER_SRGB if the draw FBO has mixed sRGB & non-SRGB colour attachments*/

vec3 EncodeBaseColor(vec3 BaseColor)
{
	/* Gamma 2.0*/
	return sqrt(saturate(BaseColor));
}

vec3 DecodeBaseColor(vec3 BaseColor)
{
	return Square(BaseColor);
}

#endif

struct GBufferInfo
{
	vec3 worldPos;

	/* 0..1*/
	vec3 BaseColor;

	/* 0..1 (derived from BaseColor, Metalness, Specular) - will computed on unpacking only*/
	vec3 DiffuseColor;
	/* 0..1 (derived from BaseColor, Metalness, Specular)- will computed on unpacking only*/
	vec3 SpecularColor;

	/* normalized*/
	vec2 WorldNormal;

	/* 0..1*/
	float Roughness;
	/* 0..1*/
	float Metallic;
	/* 0..1*/
	float Specular;

	float LinearDepth;

	float AO;
};

/** Populates GBufferInfo Packing */
void EncodeGBuffer(in GBufferInfo _GBuffer,
	out vec3 _rt0,/*Base color*/
	out vec4 _rt1,/*Material props: Metallic, Roughness, Specular, AO*/
	out vec2 _rt2,/*Normals*/
	out vec4 _rt3,/*Positions and linear depth*/
	out vec4 _rt4 /*Not used*/)
{
	/*RT0*/
	{
		_rt0.xyz = EncodeBaseColor(_GBuffer.BaseColor);
	}
	/*RT1*/
	{
		_rt1.x = _GBuffer.Metallic;
		_rt1.y = _GBuffer.Roughness;
		_rt1.z = _GBuffer.Specular;
		_rt1.w = _GBuffer.AO;
	}
	/*RT2*/
	{
		_rt2.xy = _GBuffer.WorldNormal;
	}
	/*RT3*/
	{
		/*Packing positions*/
		_rt3.xyz = _GBuffer.worldPos;
		/*Packed depth*/
		_rt3.w = _GBuffer.LinearDepth;
	}
	/*RT4*/
	{

	}
}


/** Populates GBufferInfo Unpacking */
void DecodeGBufferData(
	in sampler2D _sampler0,// gRT0
	in sampler2D _sampler1,// gRT1
	in sampler2D _sampler2,// gRT2
	in sampler2D _sampler3,// gRT3
	in sampler2D _sampler4,// gRT4
	in vec2 _coord2,
	out GBufferInfo _GBuffer)
{
	// TODO: LOD SUPPORT
	vec4 data0 = vec4(0.0);

	/*RT0*/
	{
		data0 = texture(_sampler0, _coord2);

		_GBuffer.BaseColor = data0.xyz;
		// Discard empty pixels
		if (any(lessThan(_GBuffer.BaseColor, vec3(0.0))))
		{
			discard;
		}
	}
	/*RT1*/
	{
		data0 = texture(_sampler1, _coord2);
		_GBuffer.Metallic = data0.x;
		_GBuffer.Roughness = data0.y;
		_GBuffer.Specular = data0.z;
		_GBuffer.AO = data0.w;
	}
	/*RT2*/
	{
		data0 = texture(_sampler2, _coord2);
		_GBuffer.WorldNormal = data0.xy;
		/*z, w - Empty*/
	}
	/*RT3*/
	{
		data0 = texture(_sampler3, _coord2);
		_GBuffer.worldPos = data0.xyz;
		_GBuffer.LinearDepth = data0.w;
	}

	// derived from BaseColor, Metalness, Specular
	{
		_GBuffer.DiffuseColor = _GBuffer.BaseColor - _GBuffer.BaseColor * _GBuffer.Metallic;
		_GBuffer.SpecularColor = lerp(vec3(0.08 * _GBuffer.Specular), _GBuffer.BaseColor, _GBuffer.Metallic);;
	}
}

#endif //GBUFFER_INC