[GLSL_VERTEX_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

#include "vert/eyerender.vert"

[GLSL_FRAGMENT_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

#include "frag/pcss.frag"