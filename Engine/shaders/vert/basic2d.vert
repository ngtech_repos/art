layout(location = 0) in vec3 my_Position;
layout(location = 5) in vec2 my_Texcoord0;

uniform mat4 matTexture;

//Out
out
#include "VertexData.h"

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	VERTEXDATA.v_tex_coord = (matTexture * vec4(my_Texcoord0, 0, 1)).xy;
	gl_Position = vec4(my_Position, 1);
}