layout(location = 0) in vec3 my_Position;
layout(location = 3) in vec3 my_Normal;
layout(location = 5) in vec2 my_Texcoord0;

#include "uniforms.inc"

//Out
out
#include "VertexData.h"

out gl_PerVertex
{
	vec4 gl_Position;
};

void main(void) {
	gl_Position = auto_globalMVP *(vec4(my_Position, 1.0));
#if SHADER_ENV
	VERTEXDATA.normal = normalize((auto_globalWorld * vec4(my_Normal.xyz, 0)).xyz);
#endif
	VERTEXDATA.v_tex_coord = my_Texcoord0;
}