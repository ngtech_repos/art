/**/
#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

/***/
layout(location = 0) in vec3 my_Position;
layout(location = 3) in vec3 my_Normal;
layout(location = 5) in vec2 my_Texcoord0;
layout(location = 6) in vec3 my_Tangent;
layout(location = 7) in vec3 my_Binormal;

/*Instancing*/
layout(location = 14/*GLDECLUSAGE_INSTANCE*/) in vec3 instancing_offset; // the per instance offset

out VS_OUT{
	vec3 WorldPos;
	vec3 Tangent;
	vec3 Normal;
	vec3 Bitangent;

	vec2 TexCoords;
	vec2 Depth;
	mat3 TBNmat;
} vs_out;

out gl_PerVertex
{
	vec4 gl_Position;
};

#include "depth.inc"

/**/
void main()
{
	// Calculate world-space coordinate
	vs_out.WorldPos = mat3(auto_globalWorld) * my_Position;

	vs_out.TexCoords = my_Texcoord0;

	{
		mat3 normalMatrix = mat3(auto_globalWorld);
		vec3 Normal = normalMatrix*(2.0 * my_Normal - 1.0);
		Normal = normalize(Normal);

		vs_out.Normal = Normal;
	}
	vs_out.Tangent = normalize(my_Tangent);
	vs_out.Bitangent = normalize(cross(my_Tangent, my_Normal));

	/**/
	vs_out.TBNmat = ConstructTBN(my_Normal, my_Tangent);
	// See developer-wiki: http://vk.cc/4GBBqh
	gl_Position = auto_globalMVP * vec4(vs_out.WorldPos, 1.0);
}