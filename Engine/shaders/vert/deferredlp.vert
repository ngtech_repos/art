#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

layout(location = 0) in vec3 my_Position;
layout(location = 5) in vec2 my_Texcoord0;

/*Instancing*/
layout(location = 14/*GLDECLUSAGE_INSTANCE*/) in vec3 instancing_offset; // the per instance offset

out
#include "VertexData.h"

out gl_PerVertex
{
	vec4 gl_Position;
};

void main()
{
	VERTEXDATA.v_tex_coord = my_Texcoord0;
	gl_Position = vec4(my_Position+instancing_offset, 1.0);
}