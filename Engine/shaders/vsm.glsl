[GLSL_VERTEX_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

layout(location = 0) in vec3 my_Position;

out vec4 ltov;

out gl_PerVertex
{
	vec4 gl_Position;
};

uniform mat4 lightviewproj;

void main()
{
	ltov = lightviewproj * (auto_globalWorld * vec4(my_Position, 1));
	gl_Position = ltov;
}

[GLSL_FRAGMENT_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

in vec4 ltov;
uniform vec2 clipPlanes;

out vec4 my_FragColor0;

void main()
{
	// linearized depth (handles ortho projection too)
	float d01 = (ltov.z * 0.5 + 0.5);
	float z = ((ltov.w < 0.0) ? -ltov.w : d01);
	float d = (z - clipPlanes.x) / (clipPlanes.y - clipPlanes.x);

	my_FragColor0 = vec4(d, d * d, 0.0, 0.0);
}