/*
Version: 1.0
History:
1) 2014-first version
2) 2015 rewrited as in NVSDK(See NVSDK,why gl_Position =vposition * 2.0 - 1.0. But will works and without muller
3) gl_Position = vec4(vposition.x, vposition.y, 0.0, 1.0);
*/
[GLSL_VERTEX_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

layout(location = 0) in vec4 vposition;
layout(location = 5) in vec2 vtexcoord;
out vec2 ftexcoord;

out gl_PerVertex
{
	vec4 gl_Position;
};

void main() {
	ftexcoord = vtexcoord;
	gl_Position = vec4(vposition.x, vposition.y, 0.0, 1.0);
}

[GLSL_FRAGMENT_SHADER]

#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

uniform sampler2D intexture;
in vec2 ftexcoord;
layout(location = 0) out vec4 my_FragColor0;

#define FXAA_GREEN_AS_LUMA 1
#define FXAA_EARLY_EXIT 0

#if GLSL_VERSION >= 130
#define FXAA_GLSL_130 1
#else
#define FXAA_GLSL_120 1
#endif

// FXAA_PRESET is set on C++ side
// 0:low quality but fast, .. 5: high quality but slow

// depending on the FXAA preset (formerly from 0 to 5
// we chose the FXAA settings
#if FXAA_PRESET == 0 // VeryLow
#define FXAA_QUALITY__PRESET 10
#define FXAA_PC_CONSOLE 1
#elif FXAA_PRESET == 1 // Low
#define FXAA_QUALITY__PRESET 15
#define FXAA_PC_CONSOLE 1
#elif FXAA_PRESET == 2 // Medium
#define FXAA_QUALITY__PRESET 20
#define FXAA_PC_CONSOLE 1
#elif FXAA_PRESET == 3 // High
#define FXAA_QUALITY__PRESET 20
#define FXAA_PC 1
#elif FXAA_PRESET == 4 // Very High
#define FXAA_QUALITY__PRESET 29
#define FXAA_PC 1
#elif FXAA_PRESET == 5 // Ultra
#define FXAA_QUALITY__PRESET 39
#define FXAA_PC 1
#endif

#include "Fxaa3_11.inc"

void main(void)
{	
	vec2 rcpFrame = vec2(1.0) / u_ScreenSize;
	vec2 pos = gl_FragCoord.xy / u_ScreenSize.xy;

	// Only used on FXAA Quality.
	// Choose the amount of sub-pixel aliasing removal.
	// This can effect sharpness.
	//   1.00 - upper limit (softer)
	//   0.75 - default amount of filtering
	//   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
	//   0.25 - almost off
	//   0.00 - completely off
	float QualitySubpix = 1.00;

	// The minimum amount of local contrast required to apply algorithm.
	//   0.333 - too little (faster)
	//   0.250 - low quality
	//   0.166 - default
	//   0.125 - high quality
	//   0.033 - very high quality (slower)
	float QualityEdgeThreshold = 0.033;
	float QualityEdgeThresholdMin = 0.00;

	vec4 ConsolePosPos = vec4(0.0);
	vec4 ConsoleRcpFrameOpt = vec4(0.0);
	vec4 ConsoleRcpFrameOpt2 = vec4(0.0);
	vec4 Console360RcpFrameOpt2 = vec4(0.0);
	float ConsoleEdgeSharpness = 0.0;
	float ConsoleEdgeThreshold = 0.0;
	float ConsoleEdgeThresholdMin = 0.0;
	vec4  Console360ConstDir = vec4(0, 0, 0, 0);

	my_FragColor0 = FxaaPixelShader(pos, ConsolePosPos, intexture, intexture, intexture, rcpFrame, ConsoleRcpFrameOpt, ConsoleRcpFrameOpt2, Console360RcpFrameOpt2, QualitySubpix, QualityEdgeThreshold, QualityEdgeThresholdMin, ConsoleEdgeSharpness, ConsoleEdgeThreshold, ConsoleEdgeThresholdMin, Console360ConstDir);

}