[GLSL_VERTEX_SHADER]

layout(location = 0) in vec3 my_Position;
layout(location = 3) in vec3 my_Normal;
layout(location = 5) in vec2 my_Texcoord0;
/**/
#include "shared/glslversion.h"
#include "glslcommon.inc"
#include "uniforms.inc"

out gl_PerVertex
{
	vec4 gl_Position;
};

out vec2 textureCoord;

void main()
{
	gl_Position = auto_globalMVP * vec4(my_Position, 1.0);
	textureCoord = vec2(my_Texcoord0.x, 1.0 - my_Texcoord0.y);
	// This is a quick workaround for the app-side code that requires a normal.  If you don't actually use
	// the normal here, it gets optimized away by the compiler.
	textureCoord = 0.00000001 * my_Normal.xy;
}

[GLSL_FRAGMENT_SHADER]

in vec2 textureCoord;

out vec4 my_FragColor0;

// can be default texture or texture with alpha
uniform sampler2D g_texture;

void main()
{
	if (texture(g_texture, textureCoord).w < 0.5)
		discard;

	my_FragColor0 = vec4(1.0);
}