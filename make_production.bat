@echo off
if(%1=="") echo "INCORRECT PROJECT FOLDER"
if(%2=="") echo "INCORRECT PROJECT TARGET"

echo %1
echo %2

XCOPY %~dp0\Engine  %~dp0\!production\Engine /D /E /C /R /I /K /Y

XCOPY "%1" %~dp0\!production\ /D /E /C /R /I /K /Y
COPY "%1"\system.ltx %~dp0\!production\system.ltx
COPY "%1"\user.ltx %~dp0\!production\user.ltx

  :: Call and mask out invalid call targets
  goto :selected-N-%2 2>nul || (
    :: Default case
    echo "Not selected target platform. Only Content is will copied"
  )
  goto :selected-end
  
  :selected-N-1
    echo Target_Windows Selected
XCOPY %~dp0\Bin32 %~dp0\!production\Bin32 /D /E /C /R /I /K /Y
XCOPY %~dp0\Bin64 %~dp0\!production\Bin64 /D /E /C /R /I /K /Y
    goto :selected-end     
  
  :selected-N-2
    echo Target_Linux Selected
XCOPY %~dp0\LinuxBin32 %~dp0\!production\LinuxBin32 /D /E /C /R /I /K /Y
XCOPY %~dp0\LinuxBin64 %~dp0\!production\LinuxBin64 /D /E /C /R /I /K /Y
    goto :selected-end

  :selected-N-4
    echo Target_MacOSX Selected
XCOPY %~dp0\MacBin32 %~dp0\!production\MacBin32 /D /E /C /R /I /K /Y
XCOPY %~dp0\MacBin64 %~dp0\!production\MacBin64 /D /E /C /R /I /K /Y
    goto :selected-end

  :selected-N-8
    echo Target_Android Selected
XCOPY %~dp0\AndroidBin32 %~dp0\!production\AndroidBin32 /D /E /C /R /I /K /Y
XCOPY %~dp0\AndroidBin64 %~dp0\!production\AndroidBin64 /D /E /C /R /I /K /Y
XCOPY %~dp0\AndroidBinarm32 %~dp0\!production\AndroidBinarm32 /D /E /C /R /I /K /Y
XCOPY %~dp0\AndroidBinarm64 %~dp0\!production\AndroidBinarm64 /D /E /C /R /I /K /Y
    goto :selected-end
	
  :selected-N-16
    echo Target_IOS Selected
XCOPY %~dp0\IOSBin %~dp0\!production\IOSBin /D /E /C /R /I /K /Y
    goto :selected-end
	
  :selected-N-7
    echo Target_PC Selected
	goto :selected-N-1
	goto :selected-N-2
	goto :selected-N-4
    goto :selected-end

  :selected-N-24
    echo Target_Mobile Selected
	goto :selected-N-16
	goto :selected-N-8
    goto :selected-end

  :selected-N-31
    echo Target_AllPlatforms Selected
	goto :selected-N-7
	goto :selected-N-24
    goto :selected-end

:selected-end   
del %~dp0\"!production\*.pdb"  /F /S
del %~dp0\"!production\*.bsc"  /F /S
del %~dp0\"!production\*.dmp"  /F /S
del %~dp0\"!production\*.log"  /F /S
del %~dp0\"!production\*.ipdb"  /F /S
del %~dp0\"!production\*.iobj"  /F /S
del %~dp0\"!production\*.exp"  /F /S
del %~dp0\"!production\*.lib"  /F /S
del %~dp0\"!production\*.exe.config"  /F /S
del %~dp0\"!production\*.exe.manifest"  /F /S
del %~dp0\"!production\*.vshost.exe"  /F /S
del %~dp0\"!production\*.uproj"  /F /S