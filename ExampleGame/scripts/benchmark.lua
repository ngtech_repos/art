-- benchmark status
local activity = false;
-- first frame status(int)
local first_frame = 0;
---- string name;				// benchmark name
-- benchmark scale
local scale = 1.0;
---- int flags;					// benchmark flags
---- string settings[0];			// benchmark settings
-- total time(float)
local time = 0.0;
-- total frames
local frames = 0;
-- fps time(float)
local fps_time = 0.0;		
-- fps frames(int)		
local fps_frames = 0;
-- minimum fps(float)
local min_fps = 0.0;
-- maximum fps(float)
local max_fps = 0.0;

function RunBenchmark()
    time = 0.0;
    frames = 0;

    fps_time = 0.0;
    fps_frames = 0;

    min_fps = 1000.0;
    max_fps = 0.0;

    activity = true;
    -- TODO: ��������� ��������� ������� ������� ����

    LogPrintf("Benchmark running");
end

function StopBenchmark()
    activity = false;
    --     TODO: ��������������� ��������� ������� ������� ����
    LogPrintf("Benchmark stopped");
end

function FinishedBenchmark()
    activity = false;
    LogPrintf("Benchmark results:\n");
    LogPrintf("Time:\nFrames:\t%d\nFPS:\t%g\nMin FPS:\t%g\nMax FPS:\t%g\nScore:\t%g\n" .. time .. frames .. getFps() .. min_fps .. max_fps .. getScore());
end

function getActivity()
    return activity;
end

function getFps()
    return frames / time;
end

function getScore()
    return frames / time * scale;
end

function UpdateBenchmark()

    if (activity == false) then return; end

    -- stop benchmark
    -- 	if(engine.app.clearKeyState(APP_KEY_ESC)) {
    -- 		StopBenchmark();
    -- 		return;
    -- 	}


    -- 		 check the first frame flag
    -- 		if(engine.render.isFirstFrame()) {
    -- 			first_frame = 2;
    -- 			return;
    -- 		} else if(first_frame > 0) {
    -- 			first_frame--;
    -- 			return;
    -- 		}

    -- 		// frame time
    -- 		float ftime = engine.app.getFTime();
    ftime = 1.0 / 60.0;
    time = time + ftime;
    frames = frames + 1;

    fps_frames = fps_frames + 1;
    fps_time = fps_time + ftime;
    if (fps_frames > 16) then
        local fps = fps_frames / fps_time;
        min_fps = math.min(min_fps, fps);
        max_fps = math.max(max_fps, fps);
        fps_time = 0.0;
        fps_frames = 0;
    end

    -- 		// hide mouse
    -- 		engine.gui.setMouseCursor(GUI_CURSOR_NONE);
end

function Loaded()
    --    Warning("Loaded Script");
    RunBenchmark();
    return 0;
end

function LoadedGUI()
    --    Warning("LoadedGUI Script");
    return 0;
end

function Update()
    --    Warning("Update Script");
    UpdateBenchmark();
    return 0;
end

function UpdateInput()
    -- Warning("UpdateInput Script");
    return 0;
end

function UpdateGUI()
    -- Warning("UpdateGUI Script");
    return 0;
end

function UpdatePhysics()
    --    Warning("UpdatePhysics Script");
    return 0;
end

function UpdateRender()
    -- Warning("UpdateRender Script");
    return 0;
end

function UpdateSceneManager()
    --    Warning("UpdateSceneManager Script");
    return 0;
end

function UpdateAI()
    --    Warning("UpdateAI Script");
    return 0;
end

function DestroyEvent()
    --    Warning("DestroyEvent Script");
    StopBenchmark();
    FinishedBenchmark();
    return 0;
end

function ReloadEvent()
    --    Warning("ReloadEvent Script");
    return 0;
end